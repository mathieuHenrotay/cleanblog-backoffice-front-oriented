$(function(){
  $.getScript('js/alerts.js');
  //initialisation de verif utlise pour ck editor et texteDb, valeur actuelle de l'élément dans la db
  var verif=0;
  var texteDb='';

  //fonction qui transforme un element en input en gardant sa valeur
  function transfoInput(arg){
    $(arg).removeClass('editableInput')
          .addClass('editInput');
    texteDb=$(arg).text().trim();
    $(arg).html('<input type="text" />').find('input')
                                        .val(texteDb)
                                        .focus();
  }

  //fonction qui transforme un element en ck editor
  function transfoCkEditor(arg){
    $(arg).removeClass('editable').addClass('edit');
    texteDb= $(arg).html().trim();
    $(arg).html('<textarea id="ckEditor"></textarea>').find('textarea').val(texteDb).ckeditor(function(){
      verif=1;
    });
  }

  //fonction qui remet l'element comme il etait de base dans la page
  function retourOrigine(that){
    texteDb=$(that).val();
    $(that).closest('.editInput')
            .html('')
            .text(texteDb)
            .removeClass('editInput')
            .addClass('editableInput');
  }

  //lancement de la transfo en input quand on double clique sur un element modifiable(type input) present et a venir
  $('body').on('dblclick', '.editableInput',function(){
    transfoInput(this);
  });

  //lancement de la transfo en ckEditor quand on double clique sur un element modifiable en ckeditor present et a venir
  $('body').on('dblclick', '.editable',function(){
    transfoCkEditor(this);
  });

  //mise à jour de la db pour les input modifiés
  $('body').on('change', '.editInput input', function(){
    $.ajax({
      url:'posts/edit',
      data:{
        postId:$('#postId').attr('data-id'),
        postModif:$(this).val(),
        postElement:$(this).closest('.editInput').attr('data-element')
      },
      method:'POST',
      success:function(reponsePHP){
        alerts(reponsePHP);
      }
    });
  });

  //retour au texte de base pour les inputs
  $('body').on('blur', '.editInput input', function(){
    retourOrigine(this);
  });


//écoute sur une touche
/*  $('body').on('keydown', 'input', function(event) {
    if (event.keyCode == 9) {
      alert('enter was pressed');
    }
  });*/

  //concerne ckeditor
  //on vérifie qu'on clique en dehors de l'element pour faire la maj dans la db
  //on ne peut pas faire un change car pas un input ou texte area mais iframe
  $(document).click(function(event) {
    //verif est utilisé pour limiter le nbre d'actions:
    //on ne le fait que si ck editor a été lancé
    if (verif===1) {
      //on vérifie qu'on clique en dehors de l'element pour faire la maj dans la db
      if(!$(event.target).closest('.edit').length){
        var data= $('#ckEditor').val().trim();
        //mise à jour DB si nécessaire
        if (data!==texteDb) {
          $.ajax({
            url:'posts/edit',
            data:{
              postId:$('#postId').attr('data-id'),
              postModif:data,
              postElement:$('#ckEditor').closest('.edit').attr('data-element')
            },
            method:'POST',
            success:function(reponsePHP){
              alerts(reponsePHP);
            }
          });
        }
        //retour à la normale
        $('#ckEditor').closest('.edit').html('')
                                        .html(data)
                                        .removeClass('edit')
                                        .addClass('editable');
        //réinitialisation de la variable verif
        verif=0;
      }
    }
  });

});
