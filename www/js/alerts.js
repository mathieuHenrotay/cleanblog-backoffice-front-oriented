function alerts(retour){
  if (retour>0) {
    success='<div class="alert alert-success fixed-top-right" role="alert">'
                +'Mise à jour effectuée avec succès'
                /*+'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                   +'<span aria-hidden="true">&times;</span>'
                +'</button>'*/
            +'</div>';
    $('#main').prepend(success).find('div.alert').hide().fadeIn(1200).delay(3000).fadeOut();
  }
  else {
    failure='<div class="alert alert-danger fixed-top-right" role="alert">'
                +'Impossible d\'effectuer la mise à jour'
            +'</div>';
    $('#main').prepend(failure).find('div.alert').hide().fadeIn(1200).delay(3000).fadeOut();
  }
}
