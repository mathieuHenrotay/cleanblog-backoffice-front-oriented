<?php
/*
    ./app/controleurs/pagesControleur.php
    Contrôleur des pages
 */

namespace Controleurs\Pages;
use Modeles\Page;

  function showAction(\PDO $connexion, int $id = 1){
    // Je demande la page au modèle
      include_once '../app/modeles/pagesModele.php';
      $page = Page\findOneById($connexion, $id);

    // Je charge la vue show dans $content1
      GLOBAL $content1, $title;
      $title = $page['titre'];
      ob_start();
        include '../app/vues/pages/show.php';
      $content1 = ob_get_clean();
  }

  function menuAction(\PDO $connexion){
    // Je demande la liste des pages au modèle
        include_once '../app/modeles/pagesModele.php';
        $pages = Page\findAll($connexion, 'tri');

    // Je charge la vue menu directement
        include "../app/vues/pages/menu.php";
  }
