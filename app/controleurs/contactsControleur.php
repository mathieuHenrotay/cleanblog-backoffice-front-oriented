<?php
/*
    ./app/controleurs/contactsControleur.php
    Contrôleur des contacts
 */
 namespace Controleurs\Contacts;
 use Modeles\Contact;

 function formAction(){
   ob_start();
    include '../app/vues/contacts/form.php';
    GLOBAL $content2;
   $content2=ob_get_clean();
 }

 function addDbAction(\PDO $connexion, array $params=null){
   include_once '../app/modeles/contactsModele.php';
   $lastId=Contact\insertDb($connexion, $params);

   ob_start();
    include '../app/vues/contacts/addDb.php';
    GLOBAL $content1;
  $content1=ob_get_clean();
 }
