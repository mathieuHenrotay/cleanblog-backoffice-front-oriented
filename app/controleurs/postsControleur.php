<?php
/*
    ./app/controleurs/postsControleur.php
    Contrôleur des posts
 */

namespace Controleurs\Posts;
use Modeles\Post;

  function indexAction(\PDO $connexion, array $params = []){

      // Je demande la liste des posts au modèle
        include_once '../app/modeles/postsModele.php';
        $posts = Post\findAll($connexion, $params);

      // Je charge la vue index dans $content2
        GLOBAL $content2;
        ob_start();
        include '../app/vues/posts/index.php';
        $content2 = ob_get_clean();

        GLOBAL $scripts;
        ob_start();
          include '../app/vues/scripts/posts/index.php';
        $scripts=ob_get_clean();
  }
  function showAction(\PDO $connexion, INT $id){
    include_once '../app/modeles/postsModele.php';
    $post=Post\findOneById($connexion, $id);

    ob_start();
      include '../app/vues/posts/show.php';
    GLOBAL $content1;
    $content1=ob_get_clean();

    GLOBAL $scripts;
    ob_start();
    include '../app/vues/scripts/posts/show.php';
    $scripts=ob_get_clean();
  }

  function displayOlderAction(\PDO $connexion, array $params=[]){
    include_once '../app/modeles/postsModele.php';
    $posts=Post\findAll($connexion, $params);
    include '../app/vues/posts/partials/liste.php';
  }

  function editPosts(\PDO $connexion, array $params=null){
    include_once '../app/modeles/postsModele.php';
    $success=Post\editOneById($connexion, $params);
    echo $success;
  }

  function addFormAction(){
    include '../app/vues/posts/addForm.php';
  }

  function addPostsAction(\PDO $connexion, $params=null){
    include_once '../app/modeles/postsModele.php';
    $id=Post\addOne($connexion, $params);

    include '../app/vues/posts/addPosts.php';
  }
