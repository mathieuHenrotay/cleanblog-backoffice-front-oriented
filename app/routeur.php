<?php
/*
    ./app/routeur.php
    Routeur principal
    Il décide quelle action de quel contrôleur il faut lancer
 */

  // ROUTE PAR DEFAUT
  // PATTERN: /?pageId=x
  // CTRL: pagesControleur
  // ACTION: show
    if(isset($_GET['pageId'])):
      // J'hydrate $content2 avant de l'afficher dans $content1
      switch ($_GET['pageId']) {
        case 1:
            include_once '../app/controleurs/postsControleur.php';
            Controleurs\Posts\indexAction($connexion, [
              'orderBy'   => 'datePublication',
              'orderBy2'   => 'created_at',
              'orderSens' => 'desc',
              'orderSens2' => 'desc',
              'limit' => 10
            ]);
          break;
          case 3:
            include_once '../app/controleurs/contactsControleur.php';
            Controleurs\Contacts\formAction();
      }
      // J'hydrate $content1
      include_once '../app/controleurs/pagesControleur.php';
      Controleurs\Pages\showAction($connexion,$_GET['pageId']);

    elseif(isset($_GET['postId'])):
      include_once '../app/controleurs/postsControleur.php';
      Controleurs\Posts\showAction($connexion, $_GET['postId']);

    elseif(isset($_GET['contact'])):
      include_once '../app/controleurs/contactsControleur.php';
      Controleurs\Contacts\addDbAction($connexion, [
        'nom'=>$_POST['name'],
        'email'=>$_POST['email'],
        'tel'=>$_POST['tel'],
        'message'=>$_POST['message']
      ]);

    elseif(isset($_GET['ajax'])):
      include_once '../app/controleurs/postsControleur.php';
      Controleurs\Posts\displayOlderAction($connexion, [
        'orderBy'=>'datePublication',
        'orderSens'=>'DESC',
        'orderBy2'=>'created_at',
        'orderSens2'=>'DESC',
        'limit'=>10,
        'offset'=>$_POST['offset']
      ]);

    elseif(isset($_GET['posts'])):
      switch ($_GET['posts']) {
        case 'add':
          include_once '../app/controleurs/postsControleur.php';
          \Controleurs\Posts\addPostsAction($connexion, [
            'titre'=>$_POST['postTitre'],
            'sousTitre'=>$_POST['postSousTitre'],
            'texte'=>$_POST['postTexte']
          ]);
          break;

        default:
          include_once '../app/controleurs/postsControleur.php';
          \Controleurs\Posts\editPosts($connexion, [
            'postId'=>$_POST['postId'],
            'postModif'=>$_POST['postModif'],
            'postElement'=>$_POST['postElement']
          ]);
          break;
      }
    else:
      // ROUTE PAR DEFAUT
      // PATTERN: /
      // CTRL: pagesControleur
      // ACTION: show
      include_once '../app/controleurs/postsControleur.php';
      Controleurs\Posts\indexAction($connexion, [
        'orderBy'=>'datePublication',
        'orderBy2'=>'created_at',
        'orderSens'=>'DESC',
        'orderSens2'=>'DESC',
        'limit' => 10
      ]);
      include_once '../app/controleurs/pagesControleur.php';
      Controleurs\Pages\showAction($connexion);
    endif;
