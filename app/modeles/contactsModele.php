<?php
/*
    ./app/modeles/contactsModele.php
    Modèle des contacts
 */

 namespace Modeles\Contact;

 function insertDb(\PDO $connexion, array $params){
   if($params !== null):
     $sql=  'INSERT INTO contacts
             SET nom = :nom,
                email = :email,
                tel = :tel,
                message = :message,
                dateEnvoi = NOW();';
    $rs=$connexion->prepare($sql);
    $rs->bindValue(':nom', $params['nom'], \PDO::PARAM_STR);
    $rs->bindValue(':email', $params['email'], \PDO::PARAM_STR);
    $rs->bindValue(':tel', $params['tel'], \PDO::PARAM_STR);
    $rs->bindValue(':message', $params['message'], \PDO::PARAM_STR);
    $rs->execute();
    return $connexion->lastInsertId();
   else:
     return 0;
   endif;
 }
