<?php
/*
    ./app/modeles/postsModele.php
    Modèle des posts
 */

namespace Modeles\Post;

  /*function findAll(\PDO $connexion, array $params = []){
    $params_default = [
      'orderBy' => 'id',
      'orderSens' => 'asc',
      'limit' => 999999999999999999999
    ];
    $params = array_merge($params_default, $params);

    $orderBy   = htmlentities($params['orderBy']);
    $orderBy2  = htmlentities($params['orderBy2']);
    $orderSens = htmlentities($params['orderSens']);
    $orderSens2 = htmlentities($params['orderSens2']);
    $sql = "SELECT *
            FROM posts
            ORDER BY $orderBy $orderSens, $orderBy2 $orderSens2
            LIMIT :limit ;";

    $rs = $connexion->prepare($sql);
    $rs->bindvalue(':limit', $params['limit'], \PDO::PARAM_INT);
    $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);
  }
*/
  function findOneById(\PDO $connexion, INT $id){
    $sql='SELECT *
          FROM posts
          WHERE id = :id;';
    $rs=$connexion->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_INT);
    $rs->execute();
    return $rs->fetch(\PDO::FETCH_ASSOC);
  }

  function findAll(\PDO $connexion, ARRAY $params=[]){
    $params_default=[
      'orderBy'=>'id',
      'orderSens'=>'ASC',
      'orderBy2'=>'no',
      'orderSens2'=>'no',
      'limit'=>'no',
      'offset'=>'no'
    ];
    $params= array_merge($params_default, $params);
    /*On ajoute de la sécurité car on ne peut pas préparer un ORDER BY*/
    $orderBy=htmlentities($params['orderBy']);
    $orderBy2=htmlentities($params['orderBy2']);
    $orderSens=htmlentities($params['orderSens']);
    $orderSens2=htmlentities($params['orderSens2']);
    $sql="SELECT *
          FROM posts
          ORDER BY $orderBy $orderSens";
    if ($orderBy2!=='no') {
      $sql.=", $orderBy2 $orderSens2";
    }
    if ($params['limit']!=="no") {
      $sql.=" LIMIT :limite";
    }
    if ($params['offset']!=="no") {
      $sql.=" OFFSET :pas";
    }
    $sql.=";";

    $rs=$connexion->prepare($sql);

    if ($params['limit']!=="no") {
      $rs->bindValue(':limite', intval($params['limit']), \PDO::PARAM_INT);
    }
    if ($params['offset']!=="no") {
      $rs->bindValue(':pas', intval($params['offset']), \PDO::PARAM_INT);
    }
    $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);
  }

  function editOneById(\PDO $connexion, array $params=null){
    if($params===null):
      return 0;
    else:
      $postElement=htmlentities($params['postElement']);
      $sql="UPDATE posts
            SET $postElement = :postModif,
                updated_at = NOW()
            WHERE id=:postId;"; //pas OUBLIER LE WHERE OU ON EST MORT
      $rs=$connexion->prepare($sql);
      $rs->bindValue(':postModif', $params['postModif'], \PDO::PARAM_STR);
      $rs->bindValue(':postId', intval($params['postId']), \PDO::PARAM_INT);
      return $rs->execute();
    endif;
  }

  function addOne(\PDO $connexion, $params=null){
    if($params===null){
      return 'rien dans tableau';
    }
    else {
      $sql="INSERT INTO posts
            SET titre=:titre,
                sousTitre=:sousTitre,
                texte=:texte,
                datePublication=NOW(),
                user=1,
                created_at=NOW();";
      $rs=$connexion->prepare($sql);
      $rs->bindValue(':titre', $params['titre'], \PDO::PARAM_STR);
      $rs->bindValue(':sousTitre', $params['sousTitre'], \PDO::PARAM_STR);
      $rs->bindValue(':texte', $params['texte'], \PDO::PARAM_STR);
      $rs->execute();
      return $connexion->lastInsertId();
    }
  }
