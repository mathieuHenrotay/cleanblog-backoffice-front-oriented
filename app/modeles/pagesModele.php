<?php
/*
    ./app/modeles/pagesModele.php
    Modèle des pages
 */

namespace Modeles\Page;

  function findOneById(\PDO $connexion, int $id){
    $sql = "SELECT *
            FROM pages
            WHERE id = :id;";
    $rs= $connexion ->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_INT);
    $rs->execute();
    return $rs->fetch(\PDO::FETCH_ASSOC);
  }

  function findAll(\PDO $connexion, string $orderBy = 'id'){
    $sql = "SELECT *
            FROM pages
            ORDER BY :orderBy ASC;";
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':orderBy', $orderBy, \PDO::PARAM_STR);
    $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);
  }
