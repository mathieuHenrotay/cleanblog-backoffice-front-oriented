<div class="post-preview">
  <a href="posts/<?php echo $id; ?>/<?php
    echo \Noyau\Fonctions\slugify($params['titre']); ?>">
    <h2 class="post-title">
      <?php echo $params['titre']; ?>
    </h2>
    <h3 class="post-subtitle">
      <?php echo $params['sousTitre']; ?>
    </h3>
  </a>
  <p class="post-meta">Posted on <?php echo date('Y-m-d'); ?></p>
</div>
<hr>
