<?php
/*
./app/vues/posts/show.php
variables dispos: $post=array(id, titre, sousTitre, datePublication, texte, user)
ck editor
*/
 ?>
 <header class="masthead" style="background-image: url('img/post-bg.jpg')">
   <div class="container">
     <div class="row">
       <div class="col-lg-8 col-md-10 mx-auto">
         <div class="post-heading">
           <h1 class="editableInput" data-element="titre"><?php echo $post['titre']; ?></h1>
           <h2 class="subheading editableInput" data-element="sousTitre"><?php echo $post['sousTitre']; ?></h2>
           <span class="meta">Posted on <?php echo $post['datePublication']; ?></span>
         </div>
       </div>
     </div>
   </div>
 </header>

 <!-- Textes -->
 <article id="postId" data-id="<?php echo $post['id']; ?>">
   <div class="container">
     <div class="row">
       <div class="col-lg-8 col-md-10 mx-auto">
         <!-- EDIT -->
         <div class="clearfix">
           <div class="btn btn-secondary" href="#">Double click on anything to edit &rarr;</div>
         </div>
         <div class="editable" data-element="texte">
           <?php echo $post['texte']; ?>
         </div>
       </div>
     </div>
   </div>
 </article>
