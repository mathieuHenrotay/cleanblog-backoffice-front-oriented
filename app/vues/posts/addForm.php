<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter un post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form name="addPost" id="addPostForm" novalidate method='POST' action='posts/add'>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Titre</label>
              <input type="text" class="form-control" placeholder="Titre" id="titre" name="postTitre" required data-validation-required-message="Please enter your name.">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Sous-titre</label>
              <input type="text" class="form-control" placeholder="Sous-titre" id="sousTitre" name="postSousTitre" required data-validation-required-message="Please enter your email address.">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
              <label>Texte</label>
              <textarea rows="5" class="form-control" placeholder="Corps du post" id="texte" name="postTexte" required data-validation-required-message="Please enter a message."></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <br>
          <div id="success"></div>
          <div class="form-group">
            <button type="submit" class="btn btn-secondary" id="sendMessageButton">Send</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
