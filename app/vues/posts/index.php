<?php
/*
    ./app/vues/posts/index.php
    Variables disponibles :
        - $posts ARRAY(ARRAY(id, titre, image, sousTitre, datePublication, texte, created_at, updated_at))
 */
?>
<!-- ADD A POST -->
<!--data-toggle="modal"-->
<div class="clearfix">
  <a class="btn btn-secondary float-left" id="postsAddBtn" data-toggle="modal" data-target="#exampleModal" href="#">Add Post &rarr;</a>
</div>
<?php
  include_once '../app/controleurs/postsControleur.php';
  \Controleurs\Posts\addFormAction();
?>
<!-- POSTS LIST -->
<div id="post-list">
  <?php include '../app/vues/posts/partials/liste.php' ?>
</div>

<!-- Pager -->
<div class="clearfix">
  <a id='olderPostsBtn'class="btn btn-secondary float-right" href="#">Older Posts &rarr;</a>
</div>
