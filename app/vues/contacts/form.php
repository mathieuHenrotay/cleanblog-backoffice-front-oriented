<?php
/*
ajout d'un forrmulaire de contact
variables disponibles: /////
*/
 ?>
 <form name="sentMessage" id="contactForm" novalidate method='POST' action='contacts/add'>
   <div class="control-group">
     <div class="form-group floating-label-form-group controls">
       <label>Name</label>
       <input type="text" class="form-control" placeholder="Name" id="name" name="name" required data-validation-required-message="Please enter your name.">
       <p class="help-block text-danger"></p>
     </div>
   </div>
   <div class="control-group">
     <div class="form-group floating-label-form-group controls">
       <label>Email Address</label>
       <input type="email" class="form-control" placeholder="Email Address" id="email" name="email" required data-validation-required-message="Please enter your email address.">
       <p class="help-block text-danger"></p>
     </div>
   </div>
   <div class="control-group">
     <div class="form-group col-xs-12 floating-label-form-group controls">
       <label>Phone Number</label>
       <input type="tel" class="form-control" placeholder="Phone Number" id="phone" name="tel" required data-validation-required-message="Please enter your phone number.">
       <p class="help-block text-danger"></p>
     </div>
   </div>
   <div class="control-group">
     <div class="form-group floating-label-form-group controls">
       <label>Message</label>
       <textarea rows="5" class="form-control" placeholder="Message" id="message" name="message" required data-validation-required-message="Please enter a message."></textarea>
       <p class="help-block text-danger"></p>
     </div>
   </div>
   <br>
   <div id="success"></div>
   <div class="form-group">
     <button type="submit" class="btn btn-secondary" id="sendMessageButton">Send</button>
   </div>
 </form>
