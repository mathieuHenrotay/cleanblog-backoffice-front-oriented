<?php
/*
ajout d'un forrmulaire de contact
variables disponibles: $_POST array (name, tel, email, message)
*/
?>
<!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Nous contacter</h1>
            <span class="subheading">formulaire de contact</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Textes -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="clearfix">
          <?php if ($lastId>0): ?>
            <p>Cher <?php echo $_POST['name']; ?> votre message a bien été pris en considération merci!</p>
          <?php else: ?>
            <p>Problème lors de l'envoi du formulaire, veuillez réessayer plus tard</p>
          <?php endif; ?>
        </div>
        <!-- Ici viennent les contenus complémentaires -->
        <?php
          // Soit je mets le switch Ici
          // Soit je place un $content2 qui doit être hydraté avant !!!
            GLOBAL $content2;
            echo $content2;
        ?>
      </div>
    </div>
  </div>
