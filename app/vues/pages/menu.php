<?php
/*
    ./app/vues/pages/menu.php
    Variables disponibles :
        - $pages ARRAY(ARRAY(id, titre, sousTitre, titreMenu, texte, tri))
 */
?>
<ul class="navbar-nav ml-auto">
  <?php foreach($pages as $page):?>
    <li class="nav-item">
      <a class="nav-link" href="pages/<?php echo $page['id']; ?>/<?php echo \Noyau\Fonctions\slugify($page['titre']);?>">
        <?php echo $page['titre']; ?>
      </a>
    </li>
<?php endforeach; ?>
</ul>
