<?php
/*
    ./app/vues/pages/show.php
    Variables disponibles :
        - $page ARRAY(id, titre, sousTitre, titreMenu, texte, tri)
 */
?>

<!-- Page Header -->
  <header class="masthead" style="background-image: url('img/<?php echo $page['image']; ?>')">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1><?php echo $page['titre']; ?></h1>
            <span class="subheading"><?php echo $page['sousTitre']; ?></span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Textes -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="clearfix">
          <?php echo $page['texte']; ?>
        </div>
        <!-- Ici viennent les contenus complémentaires -->
        <?php
          // Soit je mets le switch Ici
          // Soit je place un $content2 qui doit être hydraté avant !!!
            GLOBAL $content2;
            echo $content2;
        ?>
      </div>
    </div>
  </div>
