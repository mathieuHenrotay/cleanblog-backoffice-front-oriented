<?php
/*
    ./app/vues/templates/defaut.php
    Template par défaut
 */
?>
<!DOCTYPE html>
<html lang="en">

  <head>
    <?php include '../app/vues/templates/partials/head.php'; ?>
  </head>

  <body>

    <!-- Navigation -->
      <?php include '../app/vues/templates/partials/nav.php'; ?>


    <!-- CONTENU -->
    <main id="main">
      <?php echo $content1; ?>

        <hr>
    </main>
    <!-- Footer -->
    <footer>
      <?php include '../app/vues/templates/partials/footer.php'; ?>
    </footer>

    <!-- SCRIPTS -->
      <?php include '../app/vues/templates/partials/scripts.php'; ?>

  </body>

</html>
