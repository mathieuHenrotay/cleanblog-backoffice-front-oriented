<?php
/* LISTE DES FONCTIONS GENERIQUES UTILES*/
namespace Noyau\Fonctions;
function slugify($str) {
    return trim(preg_replace('/[^a-z0-9]+/', '-', strtolower($str)), '-');
}
